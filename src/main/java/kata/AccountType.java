package kata;

public enum AccountType {
    CHECKING, // Compte cheque
    SAVINGS   // Compte épargne
}

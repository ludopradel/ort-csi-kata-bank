package kata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import kata.exceptions.NotEnoughMoneyException;
import kata.exceptions.TransferNotAllowedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    void shouldCreateAccount() throws NotEnoughMoneyException, TransferNotAllowedException {
        // GIVEN
        Customer ludo = new Customer("Ludo");
        Account compteEpargne = ludo.createAccount(AccountType.SAVINGS);

        // WHEN
        compteEpargne.deposit(100.0);

        // THEN
        assertEquals(100, compteEpargne.balance());
    }

}
